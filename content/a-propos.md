---
title: À propos
date: 2022-02-20T14:56:34+01:00
menu:
  main:
    name: "À propos"
    weight: 20
featured_image: "images/slow-flowers.jpg"
---

### Qui sommes-nous ? 
Nous avons créé Ma Thèse Émoi parce que nous avons tous les quatre rencontré des difficultés pendant ces années de doctorat. Ces difficultés sont nombreuses et toutes les lister serait trop long. Voici tout de même un échantillon, que tu as peut-être malheureusement subi : 

- Des **difficultés psychologiques** : stress chronique, anxiété, dépression, surmenage, préoccupations régulières sur sa situation financière, préoccupations élevées sur son avenir professionnel...
- Des **difficultés sociales** : se sentir incompris-e par son entourage, s’isoler par manque d’énergie ou pour cause de grosse charge de travail, ambiance compétitive au sein du laboratoire, mésentente avec sa direction de thèse…
- Des **difficultés physiques** : difficultés de concentration pour cause de surmenage intellectuel ou d'insomnies répétées, maux de dos chroniques, migraines…
  
Mais ! Une thèse ne doit pas nous condamner à la souffrance car les difficultés ne sont **pas une norme** par laquelle il faut toutes et tous passer. En clair, être en doctorat ne signifie pas tout accepter, tout sacrifier (sa vie mentale et sociale). Il existe des **solutions** et des **bonnes pratiques** pour bien vivre sa thèse, que nous souhaitons développer avec chacun-e d’entre vous. Sourire pendant sa thèse ne devrait pas être une exception, une sorte de doctorant-es extra-terrestres, que chacun-e observe avec envie.

L'association a **trois objectifs** étalés sur le court, moyen et long terme : 
- Tout d'abord, nous souhaitons être un **lieu d'information**, dans lequel les doctorant-es peuvent être orienté-es en fonction de leurs besoins. Nous avons déjà constitué un annuaire thématique. Par ailleurs, nous écrivons des articles et interviewons des professionnel-les de la santé mentale afin de faire déstigmatiser les questions autour de ce sujet et d'offrir des débuts de solutions.
- Il est important de bénéficier d’une **écoute neutre et bienveillante** pour exprimer les problèmes rencontrés durant la thèse. C’est pourquoi nous souhaitons la mettre au cœur de l’association par la suite, en organisant des moments de libération de la parole et d’écoute. Il peut être apaisant de discuter avec des personnes qui traversent ou ont traversé les mêmes problèmes que soi.
- En dernier lieu, nous envisageons la tenue d'**ateliers autour du bien-être**. L’idée ici est d’apprendre à instaurer ou à cultiver une bonne hygiène de vie. Des outils accessibles au quotidien seront explorés, par exemple : la création pour se ressourcer, la respiration, la sophrologie…

Ma Thèse Émoi a été construite autour de trois valeurs :  **bienveillance**, **confidentialité** et **solidarité**. Ici tu ne seras pas jugé-e pour ce que tu traverses, pour tes décisions ou tes erreurs, pour tes hésitations et tes doutes. Chacun-e dans l’association sait que la thèse est complexe et demande beaucoup de jus de neurones et d’investissement émotionnel. Il est normal de se tromper, de se fatiguer, de rencontrer des difficultés, mais il est aussi possible de se rétablir et d’être fier-e de son travail. Nous cherchons justement à agir pour améliorer ta situation, tout en la respectant. Et ce respect implique la confidentialité car nous comprenons la sensibilité de certains sujets, ou, tout simplement, la réserve que tu peux avoir. Ainsi, aucune information te concernant ne sera mise sur la place publique. Enfin, il est évident pour nous que cette association doit permettre le développement d’une solidarité entre les doctorant-es, et entre les docteur-es et les doctorant-es, bref, de l’entraide autour de la thèse. Ma Thèse Émoi est un lieu dans lequel tu pourras échanger sur tes problèmes, apprendre des solutions et les partager – avec du thé ou un cookie, ou simplement un sourire, c’est encore mieux !

Pour finir, et parce que nous savons que vous êtes des petit-es curieuse-es, **pourquoi le nom de Ma Thèse Émoi ?** Parce que nous mettons la psychologie et les émotions au cœur de notre vocation. Nous reconnaissons l’importance de l’émotion et de ses impacts dans le quotidien des doctorant-es, et plus largement, au travail.

