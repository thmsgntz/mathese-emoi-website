---
title: "Bien-être et Nature, le regard bienveillant de l’association 'Un Pas de Côté'"
date: 2024-06-17
menu: "side" # <- ne pas toucher
description: "Cette année, nous sommes ravis de collaborer avec l’association Un Pas de Côté. Nous profitons des beaux jours qui arrivent (si, si, ils vont bien finir par arriver), pour vous emmener en nature et pour repenser notre rapport avec elle. 
Il est souvent conseillé de sortir en extérieur pour améliorer son bien-être. Pour autant, même si nous sommes conscients de cela et privilégiés à Grenoble quant à son accès, 
il n’est pas toujours évident de sortir. Capucine et Amandine nous apportent leur point de vue éclairé sur le sujet et nous offrent quelques conseils pour inclure une habitude (ou pas) de bien-être en nature. On fait les présentations ici."
tags:
  - "Nature"
  - "La Pause Doc"
toc: true # Enable Table of Contents for specific page
draft: false
---

Cette année, nous sommes ravis de collaborer avec l’association "[Un Pas de Côté](http://unpasdecoteasso.fr/)" lors de notre [1ère Pause Doc 2024](../../evenements/). 

Nous profitons des beaux jours qui arrivent (si, si, ils vont bien finir par arriver), pour vous emmener en nature et pour repenser notre rapport avec elle. 

Il est souvent conseillé de sortir en extérieur pour améliorer son bien-être. 
Pour autant, même si nous sommes conscients de cela et privilégiés à Grenoble quant à son accès, 
il n’est pas toujours évident de sortir. 

Capucine et Amandine nous apportent leur point de vue éclairé sur le sujet et 
nous offrent quelques conseils pour inclure une habitude (ou pas) de bien-être en nature. On fait les présentations ici. 

### Pouvez-vous présenter votre association ?

L’association s’appelle “Un Pas de Côté”. L'idée avec ce nom est d’inviter les participant-es à prendre du recul sur une thématique de leur vie quotidienne, de se déplacer pour voir les choses d’un autre point de vue, avec de nouvelles lunettes.

Créée en mars 2022, l'association Un Pas de Côté est le fruit d'observations, de ressentis et d'une nécessité d'agir dans un domaine précis, celui des relations que chacun et chacune entretient avec soi et avec les autres.
L'association a choisi d’agir, en s’appuyant sur les ressentis et les émotions, le milieu naturel, et leurs innombrables ressources.

Nous sommes éducatrices sportives et facilitatrices en relations humaines. Nous avons un parcours dans le milieu des activités physiques de pleine nature, avec une sensibilité pour la santé, la nature et les relations humaines !


### On peut avoir des temps de réflexion à l’intérieur d’un bâtiment, pourquoi est-il si important pour vous de réaliser des activités en extérieur ?

C’est au cours de nos études, que nous avons commencé à nous questionner sur le rapport à la nature.
L’objectif de notre association est de faire vivre des expériences en extérieur et recueillir la parole des participant-es pour mettre des mots sur ce qu'ils ressentent face à une thématique du quotidien.

Grâce aux retours des participant-es, nous avons remarqué que le lien à la nature aide à la prise de recul lorsqu’il est accompagné.
En effet, un accompagnement adapté et personnalisé permet une modification du fonctionnement du corps et de l’esprit et facilite le calme intérieur, propice à l’écoute de ses ressentis, de ses émotions.

### Selon vous, est-ce que « Nature » et « Bien-être » sont intrinsèquement liés ?

Pas toujours. Nous avons remarqué grâce à notre expérience que la nature peut créer des peurs, des angoisses. La nature peut être perçue de différentes manières selon son vécu, son expérience. Elle n’est pas toujours synonyme de bien-être. Nous pensons qu’il est important de prendre conscience de son rapport à la nature.

Pour nous, cela dépend du contexte, de l’objectif, et du cadre apporté. La nature peut être un vecteur puissant. Notre objectif est que les participant-es s’y sentent bien, afin de lier ces deux notions, et nous nous efforçons de mettre en place un cadre facilitateur et approprié, afin que le lien se fasse de manière rassurante.

### Vous allez réaliser un atelier avec Ma Thèse Émoi. Quel(s) thème(s) comptez-vous aborder et pourquoi ?

Nous ne pouvons pas tout dévoiler, l’effet de surprise est important pour nous !


Nous allons mettre en place des jeux et activités individuels et collectifs.
Au programme : ressourcement, partage et écoute sur le sujet de son propre rapport à la nature.

### Quels petits conseils pratiques pourriez-vous donner à un-e doctorant-e pour cultiver son bien-être en nature ?

Nous pensons qu’il est important d’écouter sa motivation et ses ressentis, et de ne pas se forcer pour aller en nature. Il est nécessaire de prendre conscience de son rapport à la nature, ce que signifie “nature” pour soi, de ses souvenirs (agréables, non-agréables…) en rapport avec ce sujet.
Le milieu naturel peut devenir un espace de bien-être, un espace ressourçant pour les doctorant-es.

Afin de cultiver son bien-être en nature, on peut commencer par trouver et identifier un lieu en extérieur où l’on se sent bien, sans forcément avoir besoin d’aller très loin (parc d’à côté, se mettre au soleil 10 min pour prendre une pause...). Puis observer, porter de l’attention à ses ressentis après cette pause.

### Pour aller plus loin sur le sujet, est-ce que vous avez des ressources à nous conseiller (lectures, vidéos, podcasts...) ?

Oui, sur le sujet des émotions et des ressentis :
-	Enquête et mode d'emploi - Art-Mella (bande dessinée) -	Emotions, Mode d’emploi - Christel Petitcollin (livre)
-	apprentie_girafe (instagram)


Sur	le	rapport	à la nature ainsi que les intelligences naturaliste et interpersonnelle :
-	Les intelligences multiples - Howard Gardner (livre) -	La Tribu du vivant (magazine)

Sur le questionnement, la prise de recul :
-	Philosopher et méditer avec les enfants - Frédéric Lenoir (livre)


Si vous êtes intéressées, Amandine a écrit un mémoire de recherche sur le rapport à la nature en France en comparaison avec le Chili, le Canada et les Etats-Unis. Capucine a, elle, illustré le livre : “Cultiver sa santé”, écrit par Anne Chatel, ostéopathe.

Vous pouvez nous les demander et nous vous les partagerons avec plaisir.


### Est-ce que vous avez un message ou une valeur qui vous tient à cœur et que vous souhaiteriez transmettre ici ?

Faire confiance à ses ressentis. Cultiver sa curiosité envers soi, les autres et la nature.


Pour plus d’informations sur l’association, voici le lien vers leur site internet : https://www.unpasdecoteasso.fr/accueil