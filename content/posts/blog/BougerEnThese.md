---
title: "Bouger En Thèse"
date: 2024-09-26
menu: "side" # <- ne pas toucher
description: "L’OMS nous dit qu’il n’y a pas de santé sans santé mentale.
Vous imaginez bien qu’à Ma Thèse Emoi, on en est plus que convaincu ! Mais aujourd’hui, une fois n’est pas coutume, 
nous allons aborder le sujet de la santé physique en thèse, l’un n’allant pas sans l’autre.
Être doctorant-e, c’est être beaucoup dans sa tête.
Ce travail exige une grande concentration, de nombreuses prises de décision, de l’analyse, des temps de travail prolongés...
Il n’est pas rare que le corps s  oit quelque peu oublié et reste au service de la performance intellectuelle"
tags:
  - "Témoignage"
  - "Santé"
toc: true # Enable Table of Contents for specific page
draft: false

---

> _L’OMS nous dit qu’il n’y a pas de santé sans santé mentale.
Vous imaginez bien qu’à Ma Thèse Emoi, on en est plus que convaincu ! Mais aujourd’hui, une fois n’est pas coutume, 
nous allons aborder le sujet de la santé physique en thèse, l’un n’allant pas sans l’autre.
Être doctorant-e, c’est être beaucoup dans sa tête.
Ce travail exige une grande concentration, de nombreuses prises de décision, de l’analyse, des temps de travail prolongés...
Il n’est pas rare que le corps soit quelque peu oublié et reste au service de la performance intellectuelle._ 
>
> _Cet article n’a pas vocation à vous donner une nouvelle injonction ou à vous convaincre de prendre de nouvelles bonnes résolutions, 
mais plutôt à vous donner des pistes simples et accessibles. Pour le rédiger, nous sommes allés à la rencontre de Laura*, 
professeuse de pilates et salariée (sur une chaise !). Elle a beaucoup réfléchi sur le sujet et expérimenté. 
C’est avec toute sa bienveillance qu’elle nous apporte des pistes pour aller vers un mieux-être dans son corps. 
À nous de nous les approprier comme on le souhaite._

\* Les témoignages de Laura sont en italiques dans le texte.

### Les maux du corps au travail

Avant d’aller plus loin dans la lecture de cet article, nous précisons qu’il est important de distinguer la pathologie de la gêne. 
Si vous ressentez des douleurs répétées ou fortes, il est conseillé d’aller consulter un professionnel de santé. 
Les conseils que nous dispensons ici s’appliquent à vous si vous ne souffrez pas d’une pathologie qui apporte des contre-indications. 
Ils peuvent aussi être en complément d’une démarche de soin, sans la remplacer. 
En cas de douleurs persistantes, le·a médecin généraliste est une bonne première porte d’entrée. 
Il va pouvoir prescrire des radios ou des séances de kinésithérapie, par exemple. 
Ces professionnel·les vont faire le suivi de l’évolution de ces douleurs, voir un diagnostic.

Ce qui pose problème lors de nos longues journées de travail, c’est ce qu’on appelle l’apparition des troubles musculo-squelettiques : 
les fameux TMS. Depuis 2003, on constate une augmentation de 60 % des TMS (source : [lien](https://www.ameli.fr/isere/assure/sante/themes/tms/comprendre-troubles-musculosquelettiques#:~:text=Les%20troubles%20musculo%E2%80%93squelettiques(TMS)%20regroupent%20des%20affections%20touchant,les%20membres%20inf%C3%A9rieurs%20(genoux))).

![blog-bouger-en-these-img-1.png](/images/blog/blog-bouger-en-these-img-1.jpg)

Ces troubles touchent les muscles, les articulations et les tendons, dont les zones les plus fréquemment atteintes sont le dos et les membres supérieurs (poignet, épaule, coude).
Habituellement, pour éviter l’apparition ou l’aggravation de ses troubles, il nous est recommandé de faire attention à notre posture : 
avoir nos genoux à 90°, l’écran à hauteur des yeux, par exemple. 

C’est la répétition de « mauvais » mouvements quotidiens ou de « mauvaises » postures qui vont les favoriser. 
De nombreux chercheur·ses et professionnel·les de la santé recommandent d’ajouter à cela des activités sportives adaptées à la personne (un article pour aller plus loin sur le sujet : [École du dos : prévention des lombalgies](https://www.itmp.fr/wp-content/uploads/2013/08/KS529P33.pdf)). 
Ces pratiques vont favoriser la réduction de la douleur, [tout en élargissant les capacités fonctionnelles du corps](https://www.sciencedirect.com/science/article/abs/pii/S1779012320300796).

### La bonne posture, c’est le mouvement

La première recommandation de Laura est de bouger. 
Avant d’adopter de bonnes postures (ou en complément), elle nous rappelle que l’important, c’est le mouvement.

> _« Au bureau, la base c’est que je vais essayer de me lever aussi souvent que possible. 
Quand je dis souvent, c’est toutes les heures ou toutes les demi-heures. Pas forcément longtemps. 
Aller chercher de l’eau à la cuisine, aller à l’imprimante. Ce n’est pas forcément d'aller faire une marche dehors, 
mais plutôt de faire quelques pas, parce que le plus néfaste dans nos journées de travail, c’est de rester assis sans se lever. 
C’est ça qui apporte des tensions et des douleurs »._

Laura nous rappelle également qu’il est tout à fait normal de ne pas maintenir une position parfaite tout au long de notre activité. 
Pour autant, il n’est pas nécessaire de culpabiliser, mais plutôt de **se réajuster**, de **faire une pause** et d’apporter du mouvement 
(se lever, faire des petits cercles doux avec ses épaules, s’étirer...). 

> _« Lorsqu’on est derrière son bureau, moi la première, au fil des minutes qui passent, je vais m’affaisser, mon dos va s’arrondir.
Je vais avoir les épaules qui vont tomber.
Pour moi, ce n’est vraiment pas un problème, il ne faut pas culpabiliser pour ça.
Rester tout le temps droit, ça fait mal au dos, surtout si on n’en a pas l’habitude.
Par contre, se lever très souvent, c’est efficace et c’est beaucoup moins contraignant »._

Masser, renforcer et étirer : la recette d’un corps épanoui ?
En-dehors de pathologies spécifiques, il est toujours possible de retrouver de la mobilité.
« _Ce n’est jamais fichu_ » nous dit Laura, nous pouvons élargir, ce que l’on appelle, notre « _taux de mouvement_ ».

Sauf contre-indication médicale, si l’on ressent une contracture, l’idéal est de la **masser**.
Cela peut être quelqu’un de notre entourage ou un kinésithérapeute, qui va nous masser.
Une autre solution est celle de la balle de tennis : la mettre là où l’on a mal en se positionnant contre un mur ou au sol, cela peut faire sauter des nœuds et nous aider à nous soulager. 


Si les douleurs sont légères et qu’elles ne vous « _bloquent_ » pas, l’idée est **à la fois de renforcer vos muscles et à la fois de les étirer**. 

Très souvent, c’est le fait de compenser qui va causer des douleurs. Nous sommes composés de chaînes musculaires et non pas de muscles isolés. 
Ils travaillent en synergie. C’est souvent le cas pour les douleurs cervicales, lombaires, même parfois thoraciques et dans le milieu du dos. 
Le renforcement devient alors une piste à explorer.  

Par exemple : « mes abdominaux sont faibles et lorsque je porte une charge, je fais tout supporter à mon dos et je vais créer une tension musculaire. 
Alors que lorsque notre corps est musclé dans son ensemble, lorsque l’on porte une charge,
naturellement le corps va répartir l’effort musculaire et va le répartir sur les différentes zones du corps (abdo, dos, épaules etc.). \[...] 
Si j’ai mal au dos, je vais avoir besoin de renforcer, non pas uniquement le dos, mais les jambes, les abdominaux... ». 

----
### 💡 Les conseils de Laura : Zoom sur les douleurs dans le bas du dos (lombaire)

> 3 pistes à explorer s’il n’y a pas de pathologie déclarée :
> 
> - 👉	Étirer les ischio-jambiers (à l’arrière des jambes). Souvent, des ischio-jambiers trop raides vont entraîner des douleurs plus hautes, au niveau des lombaires. On revient sur ce principe de chaînes. Ce ne sont pas les lombaires qu’il faut travailler.
> - 👉	Renforcer les fessiers.
> - 👉	L’étirement et le renforcement du muscle psoas-iliaque, responsable du fléchissement des hanches (en haut des cuisses, à l’avant du corps).
>
> D’après son expérience d’enseignante, Laura a constaté de bonnes améliorations chez ses élèves souffrant de douleurs lombaires. 
> En revanche, elle nous rappelle qu’à partir du moment où ces pratiques s’arrêtent, les douleurs reviennent. 
>
> Il s’agit bien d’instaurer une habitude, une hygiène de vie, comme un engagement envers soi, au quotidien.
----
### 💡 Les conseils de Laura : Zoom sur les douleurs dans les trapèzes (omoplates, haut du dos)

> Dans notre société, on est souvent replié vers l’avant (épaules et cou). 
> Résultats : les trapèzes à l’arrière sont sur-étirés, mais ils vont avoir besoin d’être renforcés et les pectoraux devant sont trop courts. 
> Nous allons donc avoir besoin d’étirer la partie qui est devant, ainsi que les épaules. 
> En parallèle, on va avoir besoin de renforcer la partie qui est à l’arrière.
> - 👉	Agir sur le devant, au niveau de l’ouverture de la poitrine grâce à des exercices qui vont vous aider à ouvrir et à fermer les épaules. 
> - 👉	Agir sur les muscles qui aident à descendre les épaules.
----

### Sport et douleur

On ne va pas se mentir, le sport n’est pas toujours une expérience agréable.
Qui dit effort, dit souvent douleur.
Comment repérer cette limite entre la douleur de l’effort et la douleur de la blessure ? 

**Repérer les zones douloureuses** : « _Il existe des zones dans le corps où il n’est pas bon de ressentir de la douleur.
C’est le cas des articulations, des genoux, des lombaires ou des cervicales.
Si l’on ressent une douleur de manière répétée, ce n’est pas normal et il est bon de consulter à ce moment-là pour déterminer d’où elle vient_ ». 

**Soutenir son corps en réalisant des adaptations personnalisées** : « _Si c’est le cas, en dehors des soins, les professionnels vont très certainement vous demander de réaliser des adaptations. 
Par exemple, si vous souffrez dans les lombaires lorsque vous vous penchez, une adaptation consiste à plier les genoux. 
Vous pouvez interroger les professionnel·les de santé qui vous suivent et vos enseignant·es. Lorsqu’il s’agit de notre corps, il s’agit toujours de sur-mesure_ ».

---
### 💡 Choisir son sport : zoom sur le pilates

> _« En 10 séances, vous sentirez la différence. En 20 séances, vous verrez la différence. En 30 séances, vous aurez un corps tout neuf »_ - Joseph Pilates.
> 
> _Le pilates cherche à utiliser un large spectre musculaire et à apprendre à savoir bouger en utilisant les muscles qui ont besoin d’être utilisés (donc éviter les fameuses compensations).
> Viser la régularité et accepter que les changements arrivent progressivement._
> 
> _« On parle d’un chemin dans lequel il est important de se laisser du temps. Moi, je progresse encore, je ne crois pas qu’il y ait une fin ». 
> Le plaisir de pratiquer : « Que cela soit le pilates ou un autre sport, l’important est que l’activité que l’on choisit soit plaisante pour qu’on la conserve sur la durée.
> Il est important que lorsqu’on la pratique, cela nous fasse directement du bien »._ 

---
### 💡 Zoom sur l’inclusivité dans le bien-être

> _Pour certaines personnes, il n’y a rien d’évident à pratiquer une activité physique ou à « se regarder » (intérieurement ou ressentir toutes les parties de son corps, par exemple).
Nous pensons ici aux personnes sexualisées, racisées et dont les corps sont perçus comme sortant de la norme._
> 
> _« C’est important que ces personnes aient l’envie, au-delà de l’accès, de se tourner vers elles-mêmes et de prendre soin d’elles, quelles que soient leurs conditions physiques »._
>
> _« J’ai déjà eu une élève anorexique, pour qui c’était difficile de réaliser certains exercices, comme pour une personne en surpoids.
Et c’est important aussi que ces personnes trouvent un écho sur ces pratiques de bien-être.
J’y viens, à chercher dans mon enseignement de plus en plus d’inclusivité, surtout sur le type de corps et des âges. Ce n’est pas évident, parce que pour l’enseignant ça demande aussi un travail, 
> d’inclure tous les corps et de ne pas faire sentir inapte un corps qui ne rentre pas dans la case qu’on appelle aujourd’hui « normal ». 
> Tous les corps ont droit au bien-être. On peut toujours s’adapter »._ 
>
>_Pour aller plus loin, nous vous invitons à consulter le livre de Camille Teste « Politiser le bien-être »._ 

---

### Lien mental et corps

**Pratiquer une activité physique pour éclaircir le mental**

En pilates, mais aussi dans d’autres pratiques sportives, que cela soit pour progresser ou pour ne pas se blesser, on a besoin de se concentrer sur nos mouvements,
sur notre corps, notre souffle. C’est en partie cela qui va calmer nos ruminations mentales et l’apaiser. 
Comme une pratique méditative. 

Une élève doctorante de Laura, Emma, lui a témoigné que, souvent, pendant les cours de pilates ou le soir même, 
elle avait des révélations pour sa thèse alors qu’elle était dans des moments de « _blocage_ ». 
C’est aussi le fameux « _Eurêka !_ » d’Einstein qui a une révélation alors qu’il se détend dans son bain.

**Les approches psychocorporelles et le modèle biopsychosocial comme piste de réflexion sur notre rapport au corps et au mental**

En occident, les approches psychocorporelles émergent timidement. 
Ces approches considèrent que des maux physiques peuvent trouver leur racine au niveau psychique et vice et versa. 
À titre d’exemple, un type de thérapie : la Gestalt thérapie, consiste à observer la façon dont le corps du patient réagit lorsque la parole s’exprime 
et que les émotions arrivent. Vient alors tout un travail de conscientisation et de démêlage des différentes problématiques.

Les kinésithérapeutes conçoivent de plus en plus l’intérêt à travailler sur la douleur à différents niveaux : physiques, mais aussi psychique. 
_« Une approche biopsychosociale, qui valide et tienne compte de la contribution des facteurs biologiques, psychologiques et sociaux de la douleur et de l’incapacité fonctionnelle,
est actuellement considérée comme l’approche la plus efficace en cas de douleur chronique. 
Elle est aussi considérée comme plus efficace que [des exercices ou de la kinésithérapie utilisés isolément](https://www.sciencedirect.com/science/article/abs/pii/S1779012320300796) »._

---

### 💡 Les conseils de Laura : Zoom sur le scan corporel

Laura nous présente une technique qu’elle utilise pour prendre conscience de son état du jour : le scan.
Elle conseille de l’utiliser les matins ou lorsque l’on se retrouve dans une situation difficile (prendre le temps de faire une pause, s’arrêter, se scanner et voir où on en est).

> _« L’objectif est de changer sa posture d’acteur-trice en posture d’observateur-trice.
Cela ne résout pas nécessairement les sensations difficiles, mais va permettre une conscientisation voir une légère distanciation vis-à-vis de ses émotions »._

> _« Si, par exemple, je prends conscience que je suis stressée, le stress ne va pas disparaître, mais la journée va être abordée différemment parce que j’ai pris ces 
5 minutes pour me rendre compte que je suis stressée. Et quand je vais me retrouver dans des situations qui vont titiller ou déclencher le stress, 
peut-être parce que j’ai pris conscience le matin que j’étais dans cet état-là, je vais pouvoir voir et aborder les choses différemment 
que si je subis et que je n’en prends pas conscience »._  

La façon dont cette pratique va agir est subtile. 

> _« C’est quelque chose qui va infuser, cela ne va pas être de grandes révélations. Avec le temps, cela va plus vite, on a les réflexes. 
Souvent, au début, on a besoin d’aide, de quelqu’un qui nous guide et c’est bien d’aller demander de l’aide lorsqu’on a du mal à se regarder depuis l’intérieur. 
Même si c’est commencer par une fois par semaine »._ 

- 👉	Le corps : passer mentalement en revue toutes les parties de votre corps, du sommet du crâne jusqu’au bout des doigts de pied et essayer de nommer toutes les sensations. 
Est-ce que je me sens tendu ? Est-ce que je me sens à l’aise dans mon corps ? Est-ce que je sens un manque de mobilité ou au contraire, est-ce que je me sens souple ? Léger ? Lourd ? 

- 👉	La respiration : respirer simplement et observer comment vous respirez. 
Est-ce que je me sens bloqué·e dans ma respiration ? Est-ce que j’ai l’impression de respirer pleinement ? Que ça me fait du bien ou au contraire est-ce que je sens des difficultés, que je n’aime pas me regarder respirer ? 

- 👉	Observer son état mental en tentant de le nommer.
  - Est-ce qu’aujourd’hui, je ressens de la joie, de l’anxiété, du stress, de la sérénité ? 
  - Est-ce que les pensées se bousculent dans ma tête ? 
  - Est-ce qu’un sujet en particulier revient systématiquement ?

Ce n’est pas facile et évident de se tourner vers l’intérieur et de ressentir des émotions ou des sensations douloureuses. 
Si en faisant cette observation, vous expérimentez des émotions fortes, des pensées négatives qui tournent en boucle, 
une augmentation assez grande du rythme cardiaque par exemple, c’est peut-être le signe qu’une aide extérieure est nécessaire. 
Si c’est le cas, n’hésitez pas à venir vers nous pour que l’on vous oriente ou à consulter notre annuaire. 

Si vos sensations sont supportables et que, selon les jours, vous ressentez une assez large gamme de sensations, cet exercice peut vous être grandement bénéfique. 
Pourquoi ? Parce que, qu’on les regarde ou non, ces ressentis sont bel et bien présents en nous. Faire l’effort d’être dans cette présence, dans l’idéal, 
chaque jour voire plusieurs fois par jour, constitue un point de départ intéressant vers la reliance entre le corps et le mental.

---

### Conclusion

En résumé, il est tout à fait possible d’entamer une pratique personnelle vers un mieux-être physique et une conscientisation du lien entre son corps,
son état émotionnel et ses pensées. Que cela soit par un objectif de mouvement au travail, 
la pratique d’une activité physique ou le début d’un travail de fond, médical. Ces pratiques sont, à notre sens, complètement valables et positives.

Nous terminons cet article en rappelant que parfois, l’origine du problème est extérieure à soi. 
Il peut, par exemple, être d’ordre relationnel (avec sa direction de thèse, ses collègues) ou d’ordre structurel 
(une organisation du travail interne déséquilibrée, par exemple). 
Les pratiques personnelles vont vous soutenir, mais elles ne seront peut-être pas suffisantes. 
Si vous vous retrouvez dans ce cas, sachez que des structures peuvent vous aider, ne restez pas seuls dans vos difficultés. 
Nous sommes là pour vous renseigner. 

