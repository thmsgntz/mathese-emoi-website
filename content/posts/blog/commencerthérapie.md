---
title: Commencer une psychothérapie, c'est possible !
date: 2023-04-05T14:56:34+01:00
menu: "side"

description: "Example article description"

tags:
  - "Bon à savoir"
  - "Santé"
---
*Entamer une démarche de soin en santé mentale, ce n’est pas évident. Vous avez peut-être déjà franchi beaucoup d’étapes importantes. Par exemple, vous avez été sensibilisé à l’existence des problèmes de santé mentale qui sont malheureusement courants ; vous savez que de l’aide existe ; vous avez remarqué que vous ressentez un mal-être intérieur et vous pensez que vous avez besoin d’aide. Wahou, quel chemin dans un contexte sociétal encore très tabou sur ces sujets à l’heure actuelle !
Mais maintenant voilà, comment faire pour se soigner ? Par où commencer ? Vers qui aller ? Comment en parler ? L’équipe rédactionnelle est allée à la rencontre d’une psychologue et vous a concocté un « article boussole » pour vous aider à vous repérer dans le vaste milieu de la santé mentale, voici nos conseils*

### Qu’est-ce qu’une psychothérapie ?
C’est un traitement psychologique pour une difficulté ou un problème qui entraîne une souffrance voire une détresse. C’est un outil de travail que le psychologue va mobiliser auprès d’un seul individu ou auprès d’un groupe.
Son but est de permettre à la personne d’acquérir plus d’autonomie sur la gestion de sa souffrance et de favoriser un changement significatif dans son fonctionnement qui est source de souffrance. Par exemple sur le plan cognitif : une agitation de la pensée, sur le plan émotionnel : des émotions qui débordent.
A l’heure actuelle, de nombreuses études montrent que les psychothérapies sont efficaces (Jaeken et al., 2015 ; Nadeau, 2012). Il est important toutefois de pointer un élément clé de la réussite : il s’agit de **l’engagement de la personne dans la thérapie et sa motivation à la mener**. C’est une chose importante à avoir en tête avant de commencer ce type de soin.

### Trouver son thérapeute
Pour trouver votre thérapeute, rien ne sert de vous presser ou de vous contenter d’un professionnel qui était tout simplement au-dessus de votre liste de contacts. On parle de **choix mutuel** : ce n’est pas le psy qui fait une faveur au patient de le recevoir, c’est une véritable rencontre, une relation qui demande quelques atomes crochus, notamment au niveau des valeurs. Des études ont montré que **l’alliance thérapeutique** est plus déterminante que la méthode ou les outils du thérapeute (de Roten, 2006 ; Nadeau, op. cit).
Tout comme il est essentiel de se sentir en confiance auprès de votre médecin généraliste, il est important **d’avoir confiance en votre thérapeute**. Ce qui va être dit en séance fait partie de l’intime. Il n’appartient qu’à vous de dévoiler ce que vous souhaitez sans vous sentir forcé, mais plutôt en vous sentant libre de le faire. Sur ce point, le plus efficace est de vous fier à vos ressentis. Vous pouvez quelque peu insister pour observer l’évolution de votre relation, mais si au bout de deux ou trois entretiens les ressentis négatifs persistent, vous avez le droit de changer, c’est même conseillé !
Vous pouvez commencer par **demander des recommandations à des proches** en qui vous avez confiance. Ils pourront vous raconter la façon dont travaille cette personne, témoigner sur leurs débuts avec elle, vous parler des outils mobilisés en séance… Si vous ne connaissez personne (ou que vous ne vous sentez pas de poser ce type de questions), vous pouvez **demander à des associations** de vous aiguiller. Sur Grenoble, il y a par exemple l’association Les Psy du Cœur. Une autre possibilité qui se pratique tout à fait, c’est de **réaliser un premier contact téléphonique avec un.e psychothérapeute**, de lui demander de se présenter et d’expliquer sa façon de travailler.

### Les différent-es professionnel-les de la santé mentale
* **Médecin généraliste** : comme première porte d’entrée. Il ou elle peut prescrire des arrêts de travail et certains médicaments (tranquillisant pour l’anxiété, hypnotique pour le sommeil, anti-dépresseur pour l’humeur…). Il ou elle peut conseiller des psychologues ou psychiatres.
* **Psychiatre** : médecin spécialisé en psychiatrie. Idem, il ou elle peut prescrire des arrêts de travail et par son expertise, propose des traitements médicamenteux dans un panel plus large que le médecin généraliste. Il ou elle peut également proposer des psychothérapies. Etant donné la conjoncture actuelle et le manque de psychiatre, il ou elle est à favoriser en cas de pathologie mentale importante qui entraînent des difficultés majeures dans la vie quotidienne.

Les deux ci-dessus sont entièrement remboursés par la sécurité sociale et les mutuelles.
* **Psychologue** : des études à l’université en 5 ans minimum où il ou elle est formée à la psychothérapie, à la psychopathologie, à l’histoire de la psychologie, etc. Il ou elle ne peut pas faire de prescription et se centre sur les fonctionnements psychiques lors des séances de psychothérapies. Il ou elle peut être partiellement remboursé.e ([voir notre article sur le financement des psychothérapies](https://mathese-emoi.fr/posts/blog/remboursements-mutuel/)).
* **Psychothérapeute** : depuis 2010, le terme psychothérapeute est réglementé. Les psychiatres et les psychologues sont automatiquement psychothérapeutes, du fait de leur diplôme. Quelques professionnel-les se sont formé-es uniquement à la psychothérapie et donc peuvent pratiquer en tant que psychothérapeute.
* **Psycho-praticien** : c’est le nom de métier qui succède à psychothérapeute. C’est un terme libre d’emploi qui ne constitue pas un titre, mais désigne une activité professionnelle. Certains psycho-praticiens ont une longue formation dans l’approche qu’ils ou elles proposent, d’autres n’en ont potentiellement aucune.


### Les différentes thérapies
Voici une rapide classification non exhaustive de thérapies existantes. Elles peuvent constituer un premier aiguillage pour votre choix de professionnel-le :
* **La thérapie analytique** : elle fait appel à la notion d’inconscient. Elle est centrée sur la recherche du lien entre nos difficultés actuelles et notre passé, durant la petite enfance. Chercher ce qui a été refoulé pour le mettre en conscience. Une fois cela fait, une distance peut être faite et cela modifie quelque chose dans notre fonctionnement psychique.
* **Les thérapies cognitivo-comportementales** : elles partent du principe que certaines de nos difficultés sont soit liées à nos pensées (cognitif), soit à nos comportements. Ces fonctionnements ancrés malgré nous, sont inadéquats. Elles proposent une prise de distance avec cela, sortent le problème et réfléchissent à la résolution du problème. Ensuite, il s’agit d’apprendre à remplacer les pensées et comportements dérangeants par des nouveaux mieux adaptés.
* **Les thérapies humanistes ou existentielles** : elles se fondent sur nos ressources, sur nos capacités intrinsèques en tant qu’être humain : notre manière innée de mener notre existence, d’avoir une place dans la société, d’être dans son environnement. Le but va être de mener son existence de la manière la plus satisfaisante possible. Par exemple, il existe la Gestalt thérapie, l’analyse transactionnelle, etc.
* **Les thérapies systémiques** : elles considèrent que les difficultés, les malaises que ressentent les personnes, émanent majoritairement du système social (familiale, amicale…) ou du système travail. Plus particulièrement, elles se concentrent sur les interactions à l’intérieur de ces systèmes, dans le but de modifier les relations entre l’individu et son entourage. Ce n’est pas la personne qui consulte qui a un problème, c’est davantage le système dans lequel elle évolue qui est problématique. La question ne sera pas de savoir pourquoi des problèmes existent, mais pourquoi ils persistent, se maintiennent.

### Est-ce que je suis prêt.e pour commencer une psychothérapie ?
En tant que doctorant.e, il y a beaucoup de choses à penser et à faire. Le travail est rigoureux et mobilise une grosse part de votre énergie. Se lancer dans une psychothérapie peut sembler être quelque chose de lourd, qui se rajoute encore à la longue journée de travail ou qui empiète sur les moments de détente, les temps relationnels. De plus, il est difficile d’imaginer quels vont être au juste les bénéfices. A cela, nous concluons notre article en vous apportant notre point de vue. Il est clair que la psychothérapie demande un investissement personnel. Le travail thérapeutique n’est pas une démarche aisée, car cela demande régulièrement de faire face aux peurs ou aux situations et pensées douloureuses et donc de se confronter à ce qui fait souffrir. Franchement, pourquoi s’infliger ça ? Et bien parce que les bénéfices sont bel et bien là. Plus la prise en charge se fait tôt, plus elle est efficace. La fin du doctorat supprimera sans doute des ressentis négatifs, mais sans doute pas l'entièreté. Et puis, en attendant, vous avez le droit de vous sentir à l’aise dans ce parcours et d’en profiter pleinement, sans anxiété, stress excessif, problèmes relationnels… Enfin, parce qu'un mal-être n'est pas destiné à vous accompagner tout au long de votre vie. Vous n'êtes pas résumé-e par vos souffrances et vous méritez, comme tout le monde, de vivre sans. Si vous souhaitez entamer cette démarche de soin, nous sommes là pour vous accompagner. 

*En résumé, voici quelques questions à vous poser avant de commencer un travail thérapeutique :*
1. Est-ce que je me sens motivé.e et prêt.e à m’engager dans une psychothérapie ?
2. Est-ce que des personnes de mon entourage ou des associations peuvent me conseiller un thérapeute ? 
3. Quelle part budgétaire puis-je consacrer à la psychothérapie ? Est-ce que j’ai besoin d’être remboursé.e ?
4. Est-ce que je me sens suffisamment en confiance avec mon thérapeute ? Qu’est-ce que je ressens en sa présence (par exemple, je sens que je peux tout lui dire ou au contraire je me sens trop observé et jugé ?)

Pour rappel, en cas de détresse (émotions fortes, pensées obsédantes, difficultés à y faire face), vous pouvez contacter le SAMU (15) ou le 3114 si vous avez des pensées suicidaires.

<div style="text-align: right"> *L’équipe rédac épaulée par Bénédicte JOLY*





















