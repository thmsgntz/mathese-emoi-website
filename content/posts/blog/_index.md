---
title: "Articles du blog"
page: true
menu:
  main:
    name: "Blog"
    weight: 30
--- 

### Bienvenue sur le blog de l’association ! 

Ici, tu trouveras des articles autour de la santé mentale et du bien-être en doctorat. Nous proposons : 
-    des **articles thématiques** pour t’aider concrètement à aller vers les soins et à comprendre comment le milieu de la santé mentale fonctionne (les remboursements, les types d’accompagnement, les pathologies courantes…) ; 
-    des **interviews de professionnel-les** qui expliquent le lien entre santé mentale et doctorat, d’après leurs constats et leurs expertises; 
-    des **témoignages** de doctorant-es ou de docteur-es qui racontent leurs expériences en thèse, leurs difficultés et ce qui les a aidés. 

Si d’autres sujets t’intéressent ou si tu souhaites témoigner, n’hésites pas à nous en faire part dans le **formulaire de contact**.