---
title: "Burnout : qu'est-ce que c'est et quelles solutions ?"
date: 2022-06-10T14:56:34+01:00
menu: "side" # <- ne pas toucher
description: "Example article description"
tags:
  - "Santé"
toc: true # Enable Table of Contents for specific page
draft: false
---
Le burnout ou syndrome d’épuisement professionnel (SEP) caractérise des difficultés diverses sur le plan physique (fatigue, troubles musculo-squelettiques) et psychologique (perte de sens, absence de reconnaissance). Il n’existe cependant toujours pas de consensus autour de la définition du SEP et cet article ne se fixe certainement pas comme but de rentrer et trancher une discussion épistémologique. Nous ferons plutôt une courte synthèse de différents travaux sur la question du burnout. 
<br>
### Quelques chiffres et études
Il y a malheureusement peu d’études quantitatives rigoureuses et d’ampleur sur le sujet du SEP. On peut logiquement s’en désespérer compte tenu de l’ampleur du problème (public). 

En 2015, à l’aide d’une étude couvrant la période de 2007 à 2012, l’institut de veille sanitaire a estimé que **7% des 480 000** salariés en souffrance psychologique subissent un SEP. 

En 2014, la cabinet de conseil Technologia estime quant à lui que **12% de la population active** se trouve en risque burn-out. 

Les doctorant-es sont reconnu-es comme étant une **population particulièrement susceptible de développer** un SEP (Broc et. al., 2020).

### Syndrome d’épuisement professionnel (SEP) ou burnout ou burn out ou burn-out : on parle de quoi ? 
S’il n’existe pas de définition consensuelle, il est tout de même possible de définir le SEP comme un processus long et complexe composé de quatre grandes étapes (Delgenes, Martineau-Arbes, Morat, 2021) :

**1° L’engagement** constitue une sorte de phase de **lune de miel**, dans laquelle vous vous sentez très bien au travail. Vous avez envie de vous investir dans celui-ci. En conséquence, vous ressentez de la satisfaction par rapport à cet engagement. Malgré le fait que le début de la thèse puisse être assez bordélique, c’est une joie non négligeable que de – enfin – mettre les pieds dans la cour des grand-es et de commencer à faire de la recherche. 

**2° Le surengagement** est l’étape suivante, quand le travail devient **excessif** et **compulsif**. Vous ramenez du travail chez vous et négligez les temps de repos. Vous négligez en parallèle votre santé et accumulez des signes de **surmenage** (troubles du sommeil, anxiété, etc.) et des troubles physiques divers (troubles digestifs, ORL, etc.). **Vous n’écoutez pas votre corps** qui vous dit “stop !”. Vos relations sociales – amicales, amoureuses, de famille – en pâtissent dans la mesure où vous les faites toujours passer après le travail. La thèse prend énormément de place, tant et si bien que vos dimanches ressemblent à un jour de travail comme les autres, vos soirées sont des afterwork, sans after et avec plus de work. 

**3° L’acharnement** intervient quand **vous ne maîtrisez plus** ce que vous faites, ce qui accentue votre stress. Vous vous sentez **incapable** et votre estime de soi baisse. Vous développez des **troubles physiques** supplémentaires ou ils augmentent. En réponse et en guise de défense, vous êtes tenté-e par le **cynisme** et l’**isolement** : tout ça n’est pas si important, ne vous touche pas tant que ça ET vous vous renfermez et ignorez les conseils de votre entourage qui vous demande de vous arrêter.

**4° Le burnout** est l’étape finale, l’explosion en quelque sorte, qui se manifeste par une rupture soudaine (un geste violent, une démission, une incapacité à se lever, une incapacité à réfléchir voire un suicide). Ici il n’est plus question d’ignorer, il faut **chercher de l’aide en urgence**. 

### Qu’est-ce qui cause le SEP ?
Les causes du SEP décrites ci-dessous ont été déterminées grâce au rapport d’information de la commission des affaires sociales (2017) et l’ouvrage de Delgenes, Martineau-Arbes et Morat (2021).

Tout d’abord, il ne faut surtout pas croire et donner du crédit aux discours qui font porter la responsabilité du SEP sur l’individu et sa personnalité. Selon Delgenes, Martineau-Arbes et Morat (*ibid.*), il n’existe pas de personnes fragiles mentalement, seulement des **personnes fragilisées par des structures** qui ne fournissent pas une organisation et des moyens de travail suffisants. 

Suite à la **mondialisation économique** et le **tournant néo-libéral** dans les années 1980, les entreprises françaises ont fait subir au travail une mutation pathogène : **pression concurrentielle** exacerbée, facteur humain (ou ressource humaine) comme variable d’ajustement, course effrénée à la **productivité**, **compétition** de chaque instant… Pourtant, en face, les entreprises, aussi bien privées que publiques, alors qu’elles demandent toujours plus à leurs salarié-es, ne rendent pas autant. Les salarié-es souffrent ainsi d’un **manque de reconnaissance** et de récompenses, notamment au niveau des salaires. Pire encore, avec les **nouvelles méthodes du management** il est demandé aux salarié-es de se donner corps et âmes dans l’entreprise (sur ce sujet : DE GAULEJAC Vincent, La société malade de la gestion. Idéologie gestionnaire, pouvoir managérial et harcèlement social. Paris : Edition du Seuil, 2009, 353 p). Le management recourt à des **outils qui accroissent la pression** sur les salariés : reporting, entretien individuel, évaluation continue. 

C’est pourquoi le ou la salariée est susceptible de développer un SEP quand son entreprise (privée ou publique) exige toujours plus d’elle, lui confie des tâches nombreuses dans un environnement désorganisé, sans lui donner tous les moyens de les réaliser et **compte sur son surengagement** pour qu’elle puisse les effectuer pleinement. De leur côté, les doctorant-es succombent à un burnout quand l’**université les pousse à faire sans cesse plus** (publier des articles, communiquer, donner des cours, encadrer des étudiant-es, participer à des appels à projet, s’engager dans la vie institutionnelle, tisser un réseau, chercher des financements et… enfin faire sa thèse), sans leur donner les **moyens** et sans leur offrir ni **reconnaissance** ni **perspective d’avenir**.

### Que faire ?
Pour rappel, le SEP est un processus long et complexe, qui se décline en 4 étapes (décrites plus haut). Si jamais vous atteignez l’étape 3 (acharnement) ou 4 (burnout), il faut **consulter votre médecin généraliste** rapidement. Compte tenu des dégâts causés par un SEP vous devriez être arrêté-e au moins 1 mois. Selon les besoins, vous pourrez consulter un-e **psychiatre** et un-e **psychothérapeute** ainsi que prendre un **traitement médicamenteux** pour vous aider. 
Quoi qu’il arrive, il ne faut pas retourner sur le lieu de travail sans que celui-ci soit interrogé et que les causes précises de votre SEP soient cernées. Il est important d’entamer un dialogue avec des personnes ressources (RH, direction, délégués des doctorants ou du personnel, syndicat, collègues de confiance, infirmières détachées dans la structure...) pour que la structure (publique ou privée) **prenne en compte vos besoins** et **propose des solutions concrètes satisfaisantes**. Il peut être alors envisageable de changer de direction de thèse si celle-ci est à l’origine des souffrances. Toutes ces démarches sont éprouvantes, donc n'hésitez pas à solliciter de l'aide (nous avons mis quelques associations qui peuvent vous aider plus bas)


### Prévention 
La prévention est un volet central de la lutte contre le SEP. Nous n’aborderons ici qu’une partie de la prévention, la **prévention individuelle**. Toutefois, il est évident et indispensable de porter la **prévention sur le plan collectif** ; la prévention des SEP devrait être discutée au sein de l’entreprise mais aussi dans les politiques publiques (information, formation, moyens, etc.)

**Que faut-il donc surveiller et mettre en place pour éviter un SEP ?**

Il est important de mieux **définir votre temps de travail**, notamment par rapport aux technologies numériques envahissantes. L’objectif ici est d’opérer une **séparation entre vie privée et vie professionnelle**, surtout quand cette dernière empiète sur la première – n’oublions pas qu’il existe en France un droit à la déconnexion, qui permet une “régulation de l'utilisation des outils numériques, en vue d'assurer le respect des temps de repos et de congé ainsi que de la vie personnelle et familiale” (Article L2242-17 du code du travail). Le télétravail rend cette opération d’autant plus compliquée que le lieu de travail devient le lieu de vie et inversement ; il faut alors s’assurer d’avoir des moyens dignes de travail (pour éviter des pépins physiques par exemple).

Dans le même ordre idée, il est important de **définir avec précision les missions**. Ceci est particulièrement vrai pour la thèse, où le flou a tendance à l’emporter sur la clarté, au détriment de la santé de doctorant-es qui peuvent se retrouver à travailler sur tout un tas de projets, et qui n'ont parfois rien à voir avec leur thèse. C’est pour cette raison qu’il est fortement conseillé de **discuter et négocier dès le début de la thèse** avec le directeur ou la directrice de thèse pour préciser les missions (les horaires, les moyens, les conditions de travail, les comportements attendus de part et d’autres, etc.)

Votre **entourage** a souvent des choses à dire sur votre santé : vous pouvez avoir l’air tendu, fatigué, distant, changé (négativement). Écoutez-les ! Quand vous avez la tête dans le guidon, votre entourage a le recul nécessaire pour vous signaler la nécessité de souffler. Et oui c’est difficile parce que l’entourage ne sait pas trop ce qu’est la thèse – elle peut parfois même être moquée –, mais quand vous êtes crevé-e, peu importe ce que vous faites on peut le voir. 

Ecoutez également votre corps ! Ce dernier envoie des messages sur votre état de santé. Par exemple, si vous subissez une fatigue récurrente (que même un bon sommeil ne peut réparer), c’est un signe inquiétant. Dans tous les cas, ne vous dites pas que votre entourage et votre corps exagèrent, que c’est passager, que ça va finir par passer. 

Même avant que les difficultés débordent et entachent tout votre quotidien, **parlez-en !** Vous ne pourrez pas vous en sortir seul-e ; la fierté rime mal avec SEP (en fait si, mais bon vous avez compris). Il est vrai que le Comité de Suivi Individuel est mal perçu dans la communauté doctorante, parce que vous pouvez tout à fait retrouver dans votre jury le meilleur ami de votre directeur, mais si vous en avez la possibilité, prenez le temps de parler de vos difficultés. Il existe aussi des centres de santé sur le campus et diverses associations qui peuvent vous venir en aide. Nous avons constitué pour la ville de Grenoble un [annuaire](../../../posts/annuaire/annuaire/) de structures qui viennent en aide aux doctorant-es (entre autres) sur les questions de santé mentale.


### Associations sur le burn-out
Association Stop Burn Out = https://www.assostopburnout.com/equipe-sbo
Reconstruction Post Burn Out (RPBO)  = https://www.rpbo.fr/

### Sources :
BROC Guillaume et. al., “Burnout académique en doctorat. Validation d’une échelle de burnout adaptée aux étudiants francophones en doctorat” [Document en ligne] Annales Médico-psychologiques, revue psychiatrique, n°5, 2020, pp. 517-524 : Disponible sur : https://doi.org/10.1016/j.amp.2019.01.011 (consulté en mai 2022)
COMMISSION DES AFFAIRES SOCIALES, mission d’information relative au syndrome d’épuisement professionnel (ou burn out), Rapport d’information [Document en ligne] Assemblée nationale, 2017. Disponible sur : https://www.assemblee-nationale.fr/14/rap-info/i4487.asp (consulté en mai 2022)
DELGENES Jean-Claude, MARTINEAU-ARBES Agnès, MORAT Bernard, Idées reçues sur le burn-out. Paris : Le cavalier bleu, 2021, 172 p.


