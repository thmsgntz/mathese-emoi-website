---
title: « Comment financer mes séances chez un-e psychologue ? »
date: 2022-02-20T14:56:34+01:00
menu: "side"

description: "Example article description"

tags:
  - "Bon à savoir"
  - "Santé"

thumbnail: "images/mutuelle-1.jpg" # Thumbnail image
thumbnail_alt: "Thumbnail" # alt text for thumbnail image, be screen reader friendly!
thumbnail_hide_post: false # Hide thumbnail on single post view
toc: true # Enable Table of Contents for specific page
# lead: "Exemple de sous-titre (non obligatoire)" # Lead text
---

*Aujourd’hui, les psychologues sont peu remboursés en France et on peut avoir peur d’aller consulter à cause du coût qu’entraîne ce type de soin. C’est une problématique que l’on connaît bien à Ma Thèse Emoi, on a donc cherché des solutions pour limiter ce frein. Voici ce que l’on a trouvé.*

Si votre **budget est limité**, 3 solutions s’offrent à vous :
- vous pouvez vous tourner vers une **association** qui permet d’accéder à un·e professionnel·le de la santé mentale gratuitement.
- vous pouvez choisir un psychiatre qui propose des thérapies et qui est **remboursé.**
- vous pouvez demander à votre médecin traitant de vous **prescrire une ordonnance** pour aller voir un psychologue tout en bénéficiant d’un chèque psy ou de consultations gratuites.

### Les structures autour de Grenoble pour vous accompagner
Toutes les informations sont rassemblées sur notre **[annuaire](https://mathese-emoi.fr/posts/annuaire/)**. Voici un zoom sur deux types de structure pour lesquelles vous n’avez pas besoin d’avancer de frais.
En tant que doctorant, nous vous conseillons de commencer par aller rencontrer l’un des [deux centres de santé universitaire](https://centre-sante.univ-grenoble-alpes.fr/) : **le Centre de santé du campus ou le Centre de santé du centre-ville**. Vous pouvez les appeler ou vous présenter à l’accueil en leur expliquant votre mal-être, n’hésitez pas à parler de ce que vous ressentez intérieurement. Ils sont habitués et compétents pour vous soutenir. Si vous en ressentez le besoin, vous pouvez demander à un proche de vous y accompagner, pour vous aider à « franchir le pas ».
Si vous souhaitez sortir du milieu universitaire, vous pouvez rencontrer l’association [Les Psy du Cœur](http://www.psysducoeur.fr/). Ici, **pas de prise de rendez-vous**, vous allez directement à l’association, 2 rue Pinal, pendant leur permanence le samedi matin entre 9h et 12h. La fréquentation fluctue, il y a parfois de l’attente, parfois aucune.

### Sécurité sociale selon le statut
En France, les **doctorant·es sont considéré·es comme des étudiant·es** et, à ce titre, bénéficient de la sécurité sociale. Cependant, selon votre statut (thèse financée ou pas, doctorant·e étranger·e), la situation peut changer légèrement.
Un rappel important avant toute chose, prendre une mutuelle n’est pas obligatoire en France. Dans tous les cas, vous êtes donc libres de souscrire ou non à la mutuelle de votre choix.

Pour les **thèses financées**, vous êtes affilié·e au régime de la sécurité sociale de l’université.

Pour les **thèses financées par une autre activité**, vous êtes affilié·e au régime dont dépend votre activité.*

Pour les **thèses non-financées**, en tant qu’étudiant·e, vous bénéficiez de la sécurité sociale comme n’importe quel étudiant.

Pour les **doctorant·es étranger·es**, si votre thèse est financée ou si vous financez votre thèse par une activité salariée, vous êtes automatiquement affilié·e à la sécurité sociale. Au cas où vous n’auriez pas de contrat de travail, il faudra vous déclarer sur le site Ameli. Dans tous les cas, un titre de séjour est nécessaire.

### Remboursement des séances chez un·e psychiatre par la sécurité sociale
Contrairement aux psychologues, les **psychiatres sont remboursés**, car ils ont réalisé un parcours en médecine. Ils peuvent prescrire des médicaments et également réaliser des thérapies, selon leurs spécialisations. Cependant, on attire votre attention sur **deux points de vigilance**.

Le 1er est la difficulté d’être suivi par un psychiatre. Actuellement, il y a une **pénurie de professionnel-les** et s’ajoute à cette problématique le fait que **tous ne proposent pas de psychothérapie**, se concentrant sur la médicamentation. Il faudra donc sans doute passer plusieurs coups de téléphone pour s’assurer que 1 : ils ou elles prennent des nouveaux patients (si oui quels sont les délais) et 2 : s’assurer qu’ils ou elles proposent des psychothérapies ?  

Le 2ème point de vigilance concerne les **dépassements d’honoraires**.  Pour vérifier si son psychiatre fait des dépassements, il faut vérifier s’il ou elle est conventionnée secteur 1 ou secteur 2 :
- **Secteur 1 = sans dépassement d’honoraires**. Les séances sont entièrement remboursées par la sécurité sociale et la mutuelle (même une mutuelle avec les aides de base). Exemple : consultation d’un psychiatre = 39 €. Remboursement de la sécu = 70 %, soit 27,30 € et remboursement de la mutuelle = 30% soit 11€70.
- **Secteur 2 = avec dépassement d’honoraires**. Les séances sont remboursées à la même hauteur que le secteur 1 par la sécurité, soit 27€30. Parfois les mutuelles remboursent plus, à ce moment-là, vous pouvez les contacter en amont pour vérifier le montant de leur remboursement.

Pour vérifier s’il s’agit d’un secteur 1 ou 2 :
- 1. Se rendre à l’adresse suivante : http://annuairesante.ameli.fr/
- 2. Cliquer sur un professionnel de santé
- 3. Rentrer le Nom ou la Profession ou l’Acte ainsi que la localisation

### Remboursement des séances chez un-e psychologue par la sécurité sociale
Le **coût d’une séance** chez un-e psychologue varie de 30€ à 120€ et comme vous l’imaginez sans doute, une séance ne suffit pas pour régler un problème de santé mentale. Nous parlons d’un **minimum de 6 séances** pour obtenir un résultat. En 2022, le **dispositif Mon Psy**, propose un **remboursement complet de 8 séances** chez un-e psychologue affilié-e.

Voici les **conditions** :
- Uniquement sur prescription d’un médecin généraliste
- Pas de conditions de ressources sur le remboursement
- 40 € la première séance puis 30 €
- Forfait de 8 séances à renouveler sur prescription médicale

Une **complète prise en charge** est possible via le parcours de soin proposé par les Centres médico-psychologiques ou via l’hôpital public. Les délais d’attente peuvent en revanche être assez long.

### Chèque psy ou chèque d’accompagnement psychologique pour les étudiant-es
Pour **bénéficier de ce chèque psy**, il faut être un-e étudiant-e, ce qui tombe plutôt bien pour les doctorant-es. Il faut ensuite consulter un-e généraliste (le ou la sienne, ou celui ou celle d’un centre de santé universitaire). Ce chèque donne droit à **8 consultations gratuites de 45 minutes** chez un-e psychologue, un-e psychothérapeute ou un-e psychiatre.

**Attention : pas tous et toutes les psychologues acceptent le chèque psy**, leur demander par téléphone au préalable. Vous trouverez l’explication de la démarche [ici](https://santepsy.etudiant.gouv.fr/).

### Remboursement des séances chez un-e psychologue par les mutuelles
Il existe **deux types de remboursement** :
- par **forfait annuel** : x euros remboursés chaque année
- par **séances** : x séances remboursées par an
Il est important de vérifier les conditions et clauses de votre mutuelle pour savoir à quel type de remboursement vous êtes soumis·e. Un autre élément est important à garder en tête et concerne la modification de son contrat mutuelle : il faut le faire avant le 30 novembre de l’année en cours pour une modification de l’année suivante.


En conclusion de cet article, nous te rappelons que **tu as la possibilité de nous contacter, par mail ou lors de nos permanences**, pour que l’on t’aide à trouver la formule qui te convient ou pour répondre à tes questions.

*L’équipe rédac’ de Ma Thèse Emoi*









