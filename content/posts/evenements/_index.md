---
title: "Événements"
page: true
menu:
  main:
    name: "Événements"
    weight: 5

description: "Ici tu retrouveras tous les événements proposés par Ma Thèse Émoi!"

tags:
  - "La Pause Doc"
  - "Conférences"

resources:
  name = 'header'
  src = 'images/banniere-pause-doc-octobre-2024.png'

toc: true
---

![img](/images/banniere-pause-doc-octobre-2024.png)

## 📍 Événements à venir

### 🌳🧘‍♀️ La Pause Doc -- "Edition Pilates" avec Laura Barus de l’Atelier Pilates (Octobre 2024)

👉 Les inscriptions, c'est par ici : [hello-asso](https://www.helloasso.com/associations/ma-these-emoi/evenements/la-pause-doc-edition-octobre-2024) !

#### Quoi comment où ?

À vos agendas ! À vos agendas ! Le samedi 12 Octobre de 09h à 12h, notre association Ma Thèse Émoi s'associe avec Laura Barus de l'Atelier Pilates pour vous proposer pause Pilates ☀️

Rendez-vous à la Maison du doctorat Jean Kuntzmann, Saint-Martin-d’Hères ([voir sur la carte](https://maps.app.goo.gl/C4RAt9Y2P1SEJDE46)).

Au programme : enchaînement de postures de pilate, détente du corps et boissons chaudes.

C’est un événement ouvert à tous·tes les doctorant·es ! 

👉 Les inscriptions, c'est par ici : [hello-asso](https://www.helloasso.com/associations/ma-these-emoi/evenements/la-pause-doc-edition-octobre-2024) !

![LaPaDo Automne 2024.png](/images/LaPaDo%20Automne%202024.png)


## Evénements passés 

### 🌳🐝 La Pause Doc -- "Edition Nature" avec Un Pas de Côté (Juillet 2024)

👉 Les inscriptions, c'est par ici : [hello-asso](https://www.helloasso.com/associations/ma-these-emoi/evenements/la-pause-doc-edition-juillet) !

#### Quoi comment où ?

À vos agendas ! Le mercredi 10 juillet de 18h à 20h en fin de journée, notre association Ma Thèse Émoi s'associe avec [Un Pas de Côté](http://unpasdecoteasso.fr/) pour vous proposer de prendre une pause en extérieur ☀️

Rendez-vous au parc de l’Ile d’Amour ([voir sur la carte](https://maps.app.goo.gl/wfQf9EveJHQxACkW8)) pour participer à un atelier collectif. 

Au programme : ressourcement, partage, écoute… sur le sujet du rapport à la nature.

C’est un événement ouvert à tous·tes les doctorant·es et sera suivi d’un repas partagé ⭕🍽️

#### Les intervenantes

Nous avons fait appel à [Un Pas de Côté](http://unpasdecoteasso.fr/) étant donné que l'association grenobloise propose des séjours et des ateliers en milieu naturel, met en place des débats, des temps d’échanges et des interventions au service de l'Humain. Elle accompagne les participant·es à prendre du recul sur des sujets du quotidien pour améliorer les relations que l’on entretient avec soi, les autres et l’environnement.

Amandine et Capucine, les fondatrices de l’association sont toutes deux éducatrices sportives et facilitatrices de relations humaines, sur le terrain. Il paraissaient alors évident que ce soit elles qui vous accompagnent durant cette fin de journée pour faire Pause dans le Doctorat.

👉 Les inscriptions, c'est par ici : [hello-asso](https://www.helloasso.com/associations/ma-these-emoi/evenements/la-pause-doc-edition-juillet) !

![Amandine et Capucine de l'association Un pas de côté](/images/un_pas_de_cote.jpg)


### Conférences Ma Thèse Émoi (Mai 2024)

L'audio de la conférence a été enregistrée et est disponible ici : [audioconférence](https://drive.google.com/file/d/1TFUnKCmb8qCaOE8LgWmI6M4I_q9j6KeM).

La professeure Anne Antoni nous fait l'honneur de sa présence pour parler du doctorat et 
de la souffrance qui peut graviter autour, dans notre prochaine conférence gratuite :

🗣️ "Souffrance des doctorant·es : Conditions de travail de la thèse et fonctionnement problématique de la recherche académique"

📆 Mercredi 25 mai, à 17h30

📍 A la maison du doctorat à Jean Kuntsmann

📜 Inscription obligatoire par mail ou par le [formulaire de contact](../../contact/).

[ ![images_conferences.png](/images/affiche-conf-2024-01-small.png) ](/images/affiche-conf-2024-01.png)

### La Pause Doc (Octobre 2023)

L’édition Octobre 2023 s’inscrit dans les Semaines d’information en santé mentale. Notre but est de sensibiliser à propos de la santé mentale des doctorant·es et déstigmatiser les problèmes psychologiques que ceux et celles-ci rencontrent. Ainsi, nous vous invitons - peu importe où vous en êtes dans votre thèse, quelle que soit votre discipline, ou si vous préférez le thé ou le café - à venir profiter d’une matinée de pause le samedi 28 octobre.

### Édition Octobre 2023

La prochaine Pause Doc aura lieu le 28 Octobre 2023 ! Nous te proposerons un petite déjeuner concocté par [Les Mijotées](https://www.lesmijotees.com/), puis nous passerons la matinée ensemble au cours de deux ateliers.

**Pour t'inscrire, c'est ici** : [Billeterie Hello Asso](https://www.helloasso.com/associations/ma-these-emoi/evenements/la-pause-doc-edition-2023)

#### Présentations des Ateliers

- Atelier 1 : *Bien dans son corps* avec [Julie Lopes](https://www.instagram.com/julie_lopes_phd/) 

![img](/images/pause_doc/intervenante-1-canva.png)

- Atelier 2: *bien avec soi-même* avec [Claire Dufféal](https://www.claireduffeal.fr/a-propos)

![img](/images/pause_doc/intervenante-2-canva.png)

#### Inscription

Les inscriptions se font sur HelloAsso : [Billeterie](https://www.helloasso.com/associations/ma-these-emoi/evenements/la-pause-doc-edition-2023), attention
les places sont limitées ! Saisis l'opportunité !
