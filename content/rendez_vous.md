---
title: "Rendez-vous"
date: 2024-05-03

menu:
  main:
    name: "Rendez-vous"
    weight: 5
---

Si vous avez des questions sur :

1. Votre santé mentale (exemple : des problèmes de stress ou fatigues chroniques liées à votre thèse)
2. Des soucis d'ordres relationnels (exemples : problème d'encadrement ou avec un collègue de labo),
3. Des problèmes financiers,
4. Des questions administratives,
5. Ou autres questions !

Alors, vous êtes les bienvenu·e·s ! 🥳

Nous ferons de notre mieux pour vous aiguiller vers la bonne structure / la bonne personne qui pourra vous aider 🤞

Pour cela, envoyez nous un message par notre [formulaire de contact](../contact/), nous ferons le maximum pour vous répondre dans les plus brefs délais.
