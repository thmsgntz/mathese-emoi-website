---
title: "Bienvenue"
page: true
menu:
  main:
    name: "Accueil"
    weight: 1

resources:
  name = 'header'
  src = 'images/illu-1.jpg'

--- 

Ici tu ne seras pas jugé·e pour ce que tu traverses pendant la thèse.
Notre association repose sur trois valeurs : bienveillance, confidentialité et solidarité.
Nous intervenons principalement dans la ville de Grenoble, mais si tu as des questions et que tu habites dans une autre ville, on essaiera d'y répondre également :)

## 👋 Nouvel Article : Bouger En Thèse

L’OMS nous dit qu’il n’y a pas de santé sans santé mentale. Vous imaginez bien qu’à Ma Thèse Emoi, on en est plus que convaincu !

Nous sommes allés à la rencontre de Laura, professeuse de pilates et salariée (sur une chaise !). 
C’est avec toute sa bienveillance qu’elle nous apporte des pistes pour aller vers un mieux-être dans son corps. 
A nous de nous les approprier comme on le souhaite.

C'est par ici : 👉 [Bouger En Thèse](posts/blog/bougerenthese/)

## 👋 À venir : 🌳🧘‍♀️ La Pause Doc -- "Edition Pilates" 

À vos agendas ! Le samedi 12 Octobre de 09h à 12h, 
notre association Ma Thèse Émoi s'associe avec Laura Barus de l'Atelier Pilates 
pour vous proposer pause Pilates ☀️

Plus d'information sur la page [événement](../posts/evenements/) !

![LaPaDo Automne 2024.png](/images/LaPaDo%20Automne%202024.png)