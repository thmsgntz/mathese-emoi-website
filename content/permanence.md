---
#title: "Les Permanences"
#date: 2024-02-29

#menu:
#  main:
#    name: "Permanences"
#    weight: 5
---

Si vous avez des questions sur :

1. Votre santé mentale (exemple : des problèmes de stress ou fatigues chroniques liées à votre thèse)
2. Des soucis d'ordres relationnels (exemples : problème d'encadrement ou avec un collègue de labo),
3. Des problèmes financiers,
4. Des questions administratives,
5. Ou autres questions !

Alors vous êtes les bienvenu·e·s ! 🥳

Nous ferons de notre mieux pour vous aiguiller vers la bonne structure / la bonne personne qui pourra vous aider 🤞

## Calendrier des permanences (sans rendez-vous)

Les permanences ont lieu pour le moment en ligne (voir plus bas pour le lien zoom), de 11h à 12h les jours suivants :

- 01/03

Les permanences sont accessibles à partir de ce lien [zoom](https://us06web.zoom.us/j/86817626699) (accessible les jours indiqués, de 11h à 12h).

## Avec rendez-vous

Si vous le souhaitez, vous pouvez prendre un rendez-vous avec nous sur ce lien : [calendy](https://calendly.com/mathese-emoi/60min).

Nous vous enverrons toutes les informations par mail. Le rendez-vous se passera sur Zoom.
