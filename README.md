
# Hugo Server pour le site mathese-emoi.fr

Serveur et fichiers de config pour le site [mathese-emoi.fr](mathese-emoi.fr).

Site construit avec le framework Hugo : https://gohugo.io/

Template Hugo utilisé (et adapté) : https://themes.gohugo.io/themes/hugo-dpsg/

Screenshot du site (Mai 2022) :
![img](static\images\img-site-readme.png)

# CSS

- themes\dpsg\assets\css\style.css

# Changer la couleur du thème et des titres

## Dans le config.toml

- Voir la section Params.style.vars

## Dans le CSS 

- Voir la déclaration des variables en haut 
- Et leur affectation dans les champs CSS
- Puis donner une valeur dans config.toml

# Annuaire 

## Layout

- Layout de l'index : themes\dpsg\layouts\\_default\list-annuaire.html
- Layout d'une page annuaire_* : themes\dpsg\layouts\\_default\single_annuaire.html

## Layout index grid / image / textes

Voir dans le css les champs / class suivantes :
- h2.annuaire
- img.annuaire
- .grid-container
- .grid-item

## Docx to Markdown

See : https://stackoverflow.com/a/33149947
```
 pandoc -f docx -t markdown foo.docx -o foo.markdown
```

# Sucess list pour l'installation:
- [x] Hugo Quick Start
- [x] git link to my [gitlab](https://gitlab.com/thmsgntz/mathese-emoi-website)
  - [x] Generate HTTPS token on gitlab
  - [x] git init
  - [x] git remote add origin https://gitlab.com/thmsgntz/mathese-emoi-website.git
  - [x] git push -u origin master
  
- [x] deploy on gitlabs
  - Probleme avec TOCSS et la version de hugo (voir [FAQ ici](https://gohugo.io/troubleshooting/faq/#i-get-tocss--this-feature-is-not-available-in-your-current-hugo-version)). Il faut utiliser *hugo_extended* en version et pas seulement *hugo*. Donc il faut changer la version pull par le conteneur docker de la CICD.
  - [x] `registry.gitlab.com/pages/hugo/hugo_extended:latest` au lieu de `registry.gitlab.com/pages/hugo:latest` dans .gitlab-ci.yml
  - [x] job passed!
  - [x] https://thmsgntz.gitlab.io/mathese-emoi-website/
- [x] link gitlab to domain ovh
  - https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#custom-domains 
  - https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html#2-get-the-verification-code
  - Gitlab config mathese-emoi.fr domain: https://gitlab.com/thmsgntz/mathese-emoi-website/pages/domains/mathese-emoi.fr
  - [x] Ajouter DNS OVH : "_gitlab-pages-verification-code.mathese-emoi.fr TXT gitlab-pages-verification-code=XXX"
  - [x] Attendre vérification du domaine name => OK
- [x] Let's Encrypt
  - [x] Erreur: Error “Something went wrong while obtaining the Let’s Encrypt certificate” 
    - https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html
  - [x] _Make sure your domain doesn’t have an AAAA DNS record_
    - Removes from OVH: `mathese-emoi.fr. 	AAAA 	2001:41d0:301::31`
    - Removes from OVH: `www.mathese-emoi.fr. 	AAAA 	2001:41d0:301::31`
    - Both AAAA retirés.
    - veuillez prendre en compte le temps de propagation (maximum 24h): fait à Dim 20 à 16h15.
    - [x] Wait 24h failed
  - Similar issue: https://forum.gitlab.com/t/cant-get-my-gitlab-pages-site-working-with-www-prefix/45862
    - Removes from OVH: `www.mathese-emoi.fr. 	A 	146.59.209.152` 
                        `www.mathese-emoi.fr.  	TXT 	"3|welcome"`
    - And adds: `www.mathese-emoi.fr. 	CNAME 	mathese-emoi.fr`
    - and removes: `mathese-emoi.fr. 	TXT 	"1|www.mathese-emoi.fr"`
    - Website on visibily public without "user access" ticked
    - FORGOT SOMETHING: [**For projects on GitLab.com, this IP is 35.185.44.232.**](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html#for-root-domains)
    - NOW WORKING
  - [ ] www does not work
    - Followed instructions here [For both root and subdomains](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html#for-both-root-and-subdomains) 
      - A DNS A record for the domain.
      - A DNS ALIAS/CNAME record for the subdomain.
      - A DNS TXT record for each. 

# Info OVH

- Config domain name on OVH: https://www.ovh.com/manager/web/#/domain/mathese-emoi.fr

# Resources

- Init: https://gohugo.io/getting-started/quick-start/
- Host on gitlab: https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/
- Hugo deploy: https://gohugo.io/hosting-and-deployment/hugo-deploy/

Article migration OVH->gitlab avec pas mal d'explications:
- https://anceret-matthieu.fr/2020/01/migration-de-mon-blog-wordpress-vers-hugo-sur-gitlab/

# Hugo 

Thème tranquilpeak: 
- https://github.com/LouisBarranqueiro/hexo-theme-tranquilpeak


# Theme selection :

- Thème anake: https://themes.gohugo.io/themes/gohugo-theme-ananke/
- Clean White: https://themes.gohugo.io/themes/hugo-theme-cleanwhite/ and démo : https://www.zhaohuabing.com/
- TechFeed: https://themes.gohugo.io/themes/techfeed-hugo/ and demo : https://demo.gethugothemes.com/techfeed/
- Blist: https://themes.gohugo.io/themes/blist-hugo-theme/ and demo : https://blist.vercel.app/
- Arcana: https://themes.gohugo.io/themes/hugo-arcana/ and demo : https://sec.gd/hugo/themes/arcana/
- DPSG: https://themes.gohugo.io/themes/hugo-dpsg/ and demo : https://pfadfinder-konstanz.de/ <-- Winner
- Feelit: https://themes.gohugo.io/themes/feelit/ (il a des animations CSS!) / https://feelit.khusika.com/
- Portio: https://themes.gohugo.io/themes/portio-hugo/ and demo : https://portio-hugo.staticmania.com/

Goal: Se rapprocher de : https://mcommemuses.com/

# Ajouter un formulaire

Pour l'instant, réalisé avec : https://app.getform.io/. Mais la version free ne permet que 25 soumissions...

## Formulaire de contact :

- Voir [commit](https://gitlab.com/thmsgntz/mathese-emoi-website/-/commit/0b1bb30a767ea19c40f1d8d2fc720ecadb5b35ce)
- Voir le fichier ./themes/dpsg/layouts/_default/contact.html 