
- [# Comment Contribuer](#-comment-contribuer)
- [**1. S'inscrire sur Gitlab**](#1-sinscrire-sur-gitlab)
- [**2. Les fichiers importants**](#2-les-fichiers-importants)
  - [**2.1 Les articles de blogs et l'annuaire**](#21-les-articles-de-blogs-et-lannuaire)
  - [**2.2 Les images présentes sur le site web**](#22-les-images-présentes-sur-le-site-web)
- [**3. Apporter une modification à un article existant**](#3-apporter-une-modification-à-un-article-existant)
- [**4. Créer un nouvel article de blog**](#4-créer-un-nouvel-article-de-blog)
  - [**4.1 Ajout des métadonnées**](#41-ajout-des-métadonnées)
  - [**4.2 Ajout du contenu et formattage**](#42-ajout-du-contenu-et-formattage)

# Comment Contribuer 
-----------------

## **1. S'inscrire sur Gitlab**

Se créer un compte sur [https://gitlab.com](https://gitlab.com) puis se rendre sur le projet du site web : [https://gitlab.com/thmsgntz/mathese-emoi-website](https://gitlab.com/thmsgntz/mathese-emoi-website).
Si le projet n'apparait pas, c'est que vous n'êtes pas autorisé. Demander à Thomas de vous ajouter comme contributeur :)

Vous devriez obtenir ceci comme affichage :

<img src="static\readme\how-to-accueil-gitlab.PNG" alt="img" width="1200"/>

----

## **2. Les fichiers importants** 

### **2.1 Les articles de blogs et l'annuaire**

Le contenu du blog se trouve dans le dossier `content` :

```
- content/
    |- posts/
        |- annuaire/
            |- annuaire_pro.md
            |- annuaire_dependance.md
            |- ...
        |- blog/
            |- articles_1.md
            |- articles_2.md
            |- ...
```

<img src="static\readme\how-to-contenu-gitlab.PNG" alt="img" width="1200"/>

Si on clique sur un fichier, on peut visionner son contenu, par exemple, si on va sur [*content\posts\blog\remboursements-mutuel.md*](https://gitlab.com/thmsgntz/mathese-emoi-website/-/blob/master/content/posts/blog/remboursements-mutuel.md), on voit :


<img src="static\readme\how-to-vision-article.PNG" alt="img" width="1200"/>

On retrouve bien le contnu de l'article, avec un un encard/en-tête où sont définis des métadonnées comme le "Titre", les tags, l'image à utiliser etc.

### **2.2 Les images présentes sur le site web**

Les images utilisées pour le site web sont stockées (et doivent être stocker) dans :

```
static/
    |- images/
        |- annuaire_dependance.jpg
        |- log-mte.png
        |- ...
```

-----

## **3. Apporter une modification à un article existant**

Si on revient sur notre article [*content\posts\blog\remboursements-mutuel.md*](https://gitlab.com/thmsgntz/mathese-emoi-website/-/blob/master/content/posts/blog/remboursements-mutuel.md), on peut apporter une modification en appuyant sur le bouton `Open in Web IDE`.


<img src="static\readme\how-to-vision-article-modif-1.PNG" alt="img" width="1200"/>

Une interface s'ouvre avec le contenu brut du fichier, en format [Markdown](https://www.markdownguide.org/basic-syntax/) :


<img src="static\readme\how-to-vision-article-modif-2.PNG" alt="img" width="1200"/>

C'est ici qu'on peut apporter des modifications, que ce soit sur le contenu ou sur les métadonnées.
Le markdown étant un peu difficile à prendre en main au début, surtout sur les retours à la ligne, on peut toujours venir vérifier le rendu visuel en appuyant sur le alternant entre `Edit` et `Preview Markdown` en haut : 


<img src="static\readme\how-to-vision-article-modif-3.PNG" alt="img" width="1200"/>

Aucune modification n'est prise en compte tant qu'on a pas envoyé la nouvelle version modifiée sur le serveur, pour ce faire, il faut appuyer sur le bouton `Create commit` (qui est clickable uniquement sur des modifications sont faites) : 


<img src="static\readme\how-to-vision-article-modif-4.PNG" alt="img" width="1200"/>

On arrive sur une nouvelle page qui nous montre deux éditeurs pour visionner les changements : à gauche en rouge la version précédente, à droite en vert la nouvelle version.


<img src="static\readme\how-to-vision-article-modif-5.PNG" alt="img" width="1200"/>

Pour continuer la sauvegarde, il faut se concentrer sur la partie en bas à gauche. Deux choses sont à faire :
- `Commit Message` : une petite phrase pour expliquer les changements faits. 
- `Branch` : choisir la "branch" sur laquelle envoyée les modifications. 
  - Si on choisit `Commit to master branch` : alors les changements sont directement publiés sur la version du site en ligne et seront visibles dans quelques minutes. Il faut être sûr de soi.
  - Si on choisit `Create a new branch` : alors on envoie les infos sur une branche temporaire et on peut demander à Thomas de vérifier si tout est bon avant de publier sur le site (sur master).

Une fois le bouton Commit appuyé, les modifications sont prises en compte et publiées !

Si vous voulez faire une modification en plusieurs fois. Alors il vaut mieux créer une nouvelle branche avec un nom parlant comme "encours-article-burnout". **Attention**, par défaut, les fichiers que vous voyez sont ceux sur Master. Donc vous ne verrez pas ce que vous mettez sur des branches différentes, pour les afficher, on peut choisir la branche à visionner ici (un peu tricky, ne pas hésiter à demander à Thomas) : 


<img src="static\readme\how-to-vision-article-modif-6.PNG" alt="img" width="1200"/>

Sur une autre branche que Master, vous pouvez faire ce que vous voulez, sans impacter l'état du site en ligne actuellement.

-------

## **4. Créer un nouvel article de blog**

Les articles sont à créer dans ce répertoire : [content\posts\blog](https://gitlab.com/thmsgntz/mathese-emoi-website/-/blob/master/content/posts/blog/).

Pour ajouter un fichier, il faut se rendre dans le répertoire où ajouter le fichier puis appuyer sur le bouton `+` et choisir "Add new File" : 


<img src="static\readme\how-to-new-article-1.PNG" alt="img" width="1200"/>

On arrive sur un éditeur où on peut écrire notre article, en format [Markdown](https://www.markdownguide.org/basic-syntax/). Penser à donner "`.md`" comme extension du fichier.

### **4.1 Ajout des métadonnées** 

Il faut obligatoirement ajouter des métadonnées au fichier pour que le site puisse bien l'afficher. Vous pouvez copier coller cet exemple de métadonnées, à completer (bien penser à prendre les "`---`" au début et en fin de bloc.).
Certains champs des métadonnées sont les mêmes que celui de l'article "Remboursement mutuel", pour donner un exemple d'utilisation.

Tout ce qui est après un "`#`" n'est pas pris en compte et sert de moyen pour donner des informations.

Changer la date, au moins jour, mois, année. L'heure peut rester la même.

```toml
---
title: Remboursement, mutuel et soins en thèse
date: 2022-02-20T14:56:34+01:00
menu: "side"  # <- ne pas toucher

description: "Example article description"

tags:
  - "Bon à savoir"
  - "Santé"

thumbnail: "images/mutuelle-1.jpg" # Chemin vers l'image
toc: true   # <- Ajoute table des matières en début de page
draft: true  
---
```

Le champ "draft" détermine sur l'état de l'article est un brouillon et prêt à être publié. Si draft est à true, alors il ne sera pas visible sur le site web, même si le fichier existe bien sur le serveur.

### **4.2 Ajout du contenu et formattage**

Tout ce qui arrive après les "`---`" terminant le bloc des métadonnées est considéré comme du contenu et sera affiché sur le site.

Le plus dur ici est de bien gérer le formattage de [Markdown](https://www.markdownguide.org/basic-syntax/).
Hélas lorsqu'on crée un nouveau fichier, on ne dispose pas d'option pour visualiser le rendu (le `Preview Markdown`).
Le mieux est donc de crée un fichier vide, avec le nom avec la bonne extension `.md`, `draft: true` et de venir ensuite le modifier en suivant les étapes plus haut de la section "*Apporter une modification à un article existant*"

Pour créer le fichier sur le serveur, vide ou non, il faut appuyer sur le bouton `Commit changes` en bas de la page !