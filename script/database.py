from collections import namedtuple
import sqlite3
import os
import re

DATABASE_PATH = "database/structures.db"
INFO_MD_PATH = "database/structures.md"

DB_TABLE_NAME = "Structures"
DB_LABEL_NOM = "nom"
DB_LABEL_CODE = "code"
DB_LABEL_DESCRIPTION = "description"
DB_LABEL_ADRESSE = "adresse"
DB_LABEL_TEL = "telephone"
DB_LABEL_SITE_WEB = "site_web"
DB_LABEL_EMAIL = "email"
DB_LABEL_HORAIRE = "horaire"


# Define the structure of the NamedTuple
Structure = namedtuple(
    "Structure",
    [
        "ID",
        DB_LABEL_NOM,
        DB_LABEL_CODE,
        DB_LABEL_DESCRIPTION,
        DB_LABEL_ADRESSE,
        DB_LABEL_TEL,
        DB_LABEL_SITE_WEB,
        DB_LABEL_EMAIL,
        DB_LABEL_HORAIRE,
    ],
)


def get_structures_from_database():
    # Connect to the database
    conn = sqlite3.connect(DATABASE_PATH)
    c = conn.cursor()

    # Retrieve the data from the database
    c.execute(f"SELECT * FROM {DB_TABLE_NAME}")
    rows = c.fetchall()

    # Create a dictionary of Structure NamedTuples using the data from the database
    structures = {}
    for row in rows:
        structure = Structure(*row)
        structures[structure.code] = structure

    return structures


def create_database():
    conn = sqlite3.connect(DATABASE_PATH)
    c = conn.cursor()
    c.execute(f"DROP TABLE IF EXISTS {DB_TABLE_NAME}")
    c.execute(
        f"CREATE TABLE {DB_TABLE_NAME} (ID INTEGER PRIMARY KEY, \
            {DB_LABEL_NOM} TEXT,\
            {DB_LABEL_CODE} TEXT,\
            {DB_LABEL_DESCRIPTION} TEXT,\
            {DB_LABEL_ADRESSE} TEXT,\
            {DB_LABEL_TEL} TEXT,\
            {DB_LABEL_SITE_WEB} TEXT,\
            {DB_LABEL_EMAIL} TEXT,\
            {DB_LABEL_HORAIRE} TEXT)"
    )
    conn.commit()
    conn.close()


def add_structure(nom, code, description, adresse, telephone, site_web, email, horaire):
    conn = sqlite3.connect(DATABASE_PATH)
    c = conn.cursor()
    c.execute(
        f"INSERT INTO {DB_TABLE_NAME} ({DB_LABEL_NOM},\
            {DB_LABEL_CODE},\
            {DB_LABEL_DESCRIPTION},\
            {DB_LABEL_ADRESSE},\
            {DB_LABEL_TEL},\
            {DB_LABEL_SITE_WEB},\
            {DB_LABEL_EMAIL},\
            {DB_LABEL_HORAIRE}\
            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
        (nom, code, description, adresse, telephone, site_web, email, horaire),
    )
    conn.commit()
    conn.close()


def update_structure(
    structure_id, nom, code, description, adresse, telephone, site_web, email, horaire
):
    conn = sqlite3.connect(DATABASE_PATH)
    c = conn.cursor()
    c.execute(
        f"UPDATE {DB_TABLE_NAME} SET {DB_LABEL_NOM}=?,\
             {DB_LABEL_CODE},\
             {DB_LABEL_DESCRIPTION}=?,\
             {DB_LABEL_ADRESSE}=?,\
             {DB_LABEL_TEL}=?,\
             {DB_LABEL_SITE_WEB}=?,\
             {DB_LABEL_EMAIL}=?,\
             {DB_LABEL_HORAIRE}=? \
            WHERE ID=?",
        (
            nom,
            code,
            description,
            adresse,
            telephone,
            site_web,
            email,
            horaire,
            structure_id,
        ),
    )
    conn.commit()
    conn.close()


def display_structures():
    tables_names = [
        DB_LABEL_NOM,
        DB_LABEL_CODE,
        DB_LABEL_DESCRIPTION,
        DB_LABEL_ADRESSE,
        DB_LABEL_TEL,
        DB_LABEL_SITE_WEB,
        DB_LABEL_EMAIL,
        DB_LABEL_HORAIRE,
    ]

    conn = sqlite3.connect(DATABASE_PATH)
    c = conn.cursor()
    c.execute("SELECT * FROM {DB_TABLE_NAME}")
    rows = c.fetchall()
    for row in rows:
        print(f"\tID: {row[0]}")
        for k, v in zip(tables_names, row[1:]):
            print(f"\t{k}: {v}")
        print()

    conn.close()


def populate_database_from_markdown(file_md: str = INFO_MD_PATH, delete: bool = False):

    if delete and os.path.exists(DATABASE_PATH):
        # os.remove(DATABASE_PATH)
        create_database()

    # Read the markdown file
    with open(file_md, "r") as f:
        # Parse the file line by line
        current_structure = {}

        in_header = 2
        while in_header > 0:
            if f.readline().startswith("---"):
                in_header -= 1

        for line in f:
            if len(line.strip()) == 0:
                continue
            # print(f"Parsing: '{line.strip()}'")

            # If the line starts with a "#", it indicates the start of a new structure
            if line.startswith("#"):
                # If we are already processing a structure, insert it into the database
                if current_structure:
                    add_structure(
                        nom=current_structure.get("Nom", "NA"),
                        code=current_structure.get("Code", "NA"),
                        description=current_structure.get("Description", "NA"),
                        adresse=current_structure.get("Adresse", "NA"),
                        telephone=current_structure.get("Téléphone", "NA"),
                        site_web=current_structure.get("Site internet", "NA"),
                        email=current_structure.get("Email", "NA"),
                        horaire=current_structure.get("Horaires", "NA"),
                    )
                    current_structure = {}

                # Extract the structure name from the line
                current_structure["Nom"] = line[1:].strip()
            else:
                # Split the line into a key-value pair
                key, value = line.split(":", 1)
                # Strip leading and trailing whitespace and store the value in the current structure
                current_structure[key.strip()] = value.strip()
        # Insert the last structure into the database
        add_structure(
            nom=current_structure.get("Nom", "NA"),
            code=current_structure.get("Code", "NA"),
            description=current_structure.get("Description", "NA"),
            adresse=current_structure.get("Adresse", "NA"),
            telephone=current_structure.get("Téléphone", "NA"),
            site_web=current_structure.get("Site internet", "NA"),
            email=current_structure.get("Email", "NA"),
            horaire=current_structure.get("Horaires", "NA"),
        )


def replace_tags_in_files(structures, directory):
    # Compile the regex pattern to match tags in the form [TAG]
    pattern = re.compile(r'\[(.*?)\]')
    print(f"DEBUG RUNNING ON {directory}")

    # Walk through the directory and its subdirectories
    for root, _, files in os.walk(directory):
        for file in files:
            # Only process text files
            print(f"Process {file}")
            if file.endswith(".md") or file.endswith(".html"):
                file_path = os.path.join(root, file)
                with open(file_path, "r") as f:
                    # Read the file contents
                    contents = f.read()
                # Find all tags in the contents
                for match in pattern.finditer(contents):
                    tag = match.group(1)
                    # If the tag corresponds to a structure code, replace it with the structure information
                    if tag in structures:
                        print(f"FOUND {tag} in {file}")
                        structure = structures[tag]
                        replacement = pretty_structure_print(structure)
                        contents = contents.replace(match.group(0), replacement)
                # Write the modified contents back to the file
                with open(file_path, "w") as f:
                    f.write(contents)


def pretty_structure_print(s: namedtuple):
    ds = s._asdict()
    for k, v in ds.items():
        if v == "NA":
            ds[k] = "-"
    return f"""### {ds[DB_LABEL_NOM]}

Description : {ds[DB_LABEL_DESCRIPTION]}

Adresse : {ds[DB_LABEL_ADRESSE]}

Horaire : {ds[DB_LABEL_HORAIRE]}

Téléphone : {ds[DB_LABEL_TEL]}

Site internet : {ds[DB_LABEL_SITE_WEB]}

Email : {ds[DB_LABEL_EMAIL]}

    """


if __name__ == "__main__":
    populate_database_from_markdown(delete=True)
    # display_structures()
    s = get_structures_from_database()
    # print(s.keys())
    # replace_tags_in_files(s, "public/posts/annuaire/")
    replace_tags_in_files(s, "content/posts/")