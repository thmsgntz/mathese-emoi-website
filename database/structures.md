---
Base de données des structures.
Pour renseigner une nouvelle structure, on commence par le nom de la structure précédée en début de ligner par '#'.

Ensuite on ajoute des informations en commençant une ligne par le type d'information.
Types possibles => Code, Description, Adresse, Téléphone, Horaires, Site internet, Email, Horaires

Seul le nom et le code sont obligatoires à renseigner.

Le code est à définir de façon arbitraire, c'est celui qu'on renseignera dans le site pour savoir où faire apparaître les infos d'une structure. (par défaut=> prendre les initiales)

Ne pas revenir à la ligne après l'utilisation de des deux points

Exemple =>

# Nom de la structure exemple

Code: NSE01
Description: ceci est la structure pour illustrée l'exemple
Téléphone: 0102030405
Horaires: de 12h à 12h15
---

# Bulle d'air
Code: BA01-FR
Description : Propose du soutien et un espace de bienveillance pour parler librement. Il faut prendre rendez-vous sur le site Site affluence. 

Adresse : Maison du doctorat Jean Kuntzmann 110 rue de la chimie 38400 Saint-Martin-d'Hères 

Site internet : https://doctorat.univ-grenoble-alpes.fr/pendant-la-these/social-et-lutte-contre-les-comportements-inappropries-/bulle-d-air-le-dispositif-d-ecoute-active-et-bienveillante-1115177.kjsp



# Centre de santé du campus

Code: CSC01-FR
Description : Propose des soins, notamment en psychologie, sophrologie et relaxation.

Adresse : 80 allée Ampère 38400 St Martin d’Hères

Téléphone : 04-76-18-79-79 

Horaires : 8h à 17h les lundi, mercredi, jeudi et vendredi, 10h à 17h le mardi

Site internet : https://centre-sante.univ-grenoble-alpes.fr/

# Centre de santé du centre ville de Grenoble

Code: CSCVG01-FR
Adresse : 10 rue Vassieux en Vercors 38000 Grenoble

Téléphone : 04-76-82-40-70

Horaires : 8h à 17h les lundi, 13h30 à 17h le mardi

# CLIPS

Code: CLIPS01-FR
Description : Propose un accueil par des professionel·les formés à l’écoute, des entretiens avec des psys cliniciens, des groupes d’expression animés par des expert·es dans leur domaine.
Adresse : 1 place de l’Etoile 38000 Grenoble
Téléphone : 04-76-87-90-45
Site internet : https://www.clips38.com/

# Les psys du coeur

Code : LPDC01-FR

Description : Propose un soutien thérapeutique à toute personne éloignée des consultations traditionnelles pour des raisons sociales, économiques et/ou culturelles.

Adresse : 2 Rue Pinal 38000 Grenoble

Email : grenoble@psysducoeur.fr

Téléphone : 07-63-47-42-14

Site internet : http://www.psysducoeur.fr/

# Service Médico-Psychologique Universitaire

Code : SMPU01-FR

Description : Propose des consultations et des soins psychothérapiques avec des psychiatres et des psychologues. Pour prendre rdv, il faut s'adresser au secrétariat sur place et exprimer sa demande. Le service est gratuit.

Adresse : 8 place du Conseil National de la Résistance

Téléphone : 04-56-58-82-70

Horaires : lundi au vendredi de 8h30 à 16h

Site internet : https://etudiant.univ-grenoble-alpes.fr/sante-et-bien-etre/prendre-soin-de-votre-sante-mentale/prendre-soin-de-votre-sante-mentale-1354888.kjsp

# CAARUD AIDES (Centre d'accueil et d'accompagnement à la réduction des risques des usagers de drogues)

Code: CAARUD01-FR

Description : Accueil collectif et individuel, l'information et le conseil personnalisé pour usager-es de drogues

Adresse : 8 rue Sergent Bobillot 38000 Grenoble

Téléphone : 04-76-47-36-46

# Drogues Info Service

Code : DIS01-FR

Description : Service national d'aide à distance en matière de drogues et de dépendance (anonyme et gratuit)

Horaires : 7j/7 de 8h à 2h

Téléphone : 0-800-23-13-13

Site internet : http://www.drogues-info-service.fr/

# Tabac Info Service

Code : TIS01-FR

Description : Informations et aide dans la démarche d'arrêt du tabac.

Horaires : du lundi au samedi de 8h à 20h

Téléphone : 39-89

Site internet : http://www.tabac-info-service.fr/

# JALMALV

Code : JALMALV01-FR

Description : Propose des entretiens individuels et des groupes d’entraide pour parler du deuil

Adresse : 4 bis Rue Hector Berlioz 38000 Grenoble

Téléphone : 04-76-51-08-51

Email : asso-jalmalv.grenoble@orange.fr

Site internet : https://www.jalmalv-grenoble.fr/

# Le Caméléon (EN)

Code : CAMELE01-FR

Description : Mental health and migration. Two possible support systems: individual care (psychological or psycho-corporal), group care (body or artistic Meditation).

Adresse : 16 rue Aimée Berey, 38000 Grenoble, France

Email : contact@asso-le-cameleon.org

Site internet : http://asso-le-cameleon.org/

# Apsytude

Code : APSY01-FR

Description : Prévenir le mal-être et ses conséquences notamment en déstigmatisant le recours aux psychologues. Prendre en charge la souffrance en rendant accessible à toutes et tous la consultation psychologique. Promouvoir le bien-être en donnant aux étudiant-es des outils leur permettant de devenir acteurs et actrices de leur santé.
Prendre RDV sur leur site: https://www.apsytude.com/fr/

Site internet : https://www.apsytude.com/fr/apsytude/contact/


# Suicide écoute

Code : SUIC01-FR

Description : Propose une écoute 24h/24 et 7j/7

Téléphone : 01-45-39-40-00

Site internet : https://www.suicide-ecoute.fr/

# 3114, numéro nationale de prévention suicide

Code : NNPS01-FR

Horaires : 24h/24 et 7j/7

Description : Que cela soit pour soi ou pour un proche qui pense au suicide.

Téléphone : 31-14

# Alpaline

Code : ALPA01-FR

Description : Ligne d’écoute tenue par des étudiant-es formé-es pour lutter contre l’isolement. //Alpaline est fermée pour l'été et repense ses activités pour la rentrée prochaine.

Téléphone : 04-65-84-44-24

Horaires : Du vendredi au lundi de 20h à 23h

Site internet : https://www.instagram.com/alpaline_iaga/

# La bienvenue

Code : BIENV01-FR

Description : Propose un lieu d’accueil et d’écoute, ainsi que des activités, de la documentation et des informations sur des événements et des lieux de solidarité. 

Adresse : 8 Rue Frédéric Taulier, 38000 Grenoble

Téléphone : 04-38-38-00-20

Horaires : lundi au vendredi de 15h à 18h

Site internet : https://www.la-bienvenue.fr/



# SOS Amitié

Code : SOSA01-FR

Description : Service d’écoute destiné à accueillir la parole de celles et ceux qui, à un moment de leur vie, traversent une période difficile (confidentiel, anonyme et gratuit)

Horaires : 7j/7 et 24h/24

Téléphone : 09-72-39-40-50

Site internet : www.sos-amitie.com


# Soutien psychologique Auvergne-Rhône-Alpes

Code : SPARA01-FR

Description : Service d'écoute téléphonique pour les étudiants, par un psychologue (confidentiel, anonyme et gratuit)

Horaires : 7j/7 et 24h/24

Téléphone : 04-26-73-32-32
